#!/bin/bash
cp -rfu . /var/lib/docker/volumes/api__data/_data/

docker exec $(docker ps | grep api_php | awk '{print $11}') composer install
