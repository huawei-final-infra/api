#!/bin/bash

server="d2"

ssh $server git -C api pull origin develop/anc
ssh $server rm -rf /var/lib/docker/volumes/api__data/_data/*
rsync -vrd * .devcontainer  .editorconfig  .env  .env.test  .git  .github  .gitignore  .php-cs-fixer.dist.php  d2:/var/lib/docker/volumes/api__data/_data/

ssh $server api/entrypoint.sh
